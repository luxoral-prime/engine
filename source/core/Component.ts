/** The class all Components extend */
export abstract class Component {}

/** The constructor for all Components */
export interface ComponentConstructor<T extends Component> {
    new (...args: any): T;
}
