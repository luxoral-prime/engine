import { Scene } from "./Scene";

export interface EngineOptions {
    scene?: Scene;
}

/** The main Engine class that controls scenes, updating etc */
export class Engine {
    private _running: boolean;
    private _frame: number;
    private _delta: number;
    private _fps: number;
    private _lastUpdate: number;
    private rafId: number;

    /** The current Scene */
    public scene: Scene | undefined;

    /** Returns true if the Engine is running (has been started) */
    public get running(): boolean {
        return this._running;
    }

    /** Returns the total amount of frames rendered */
    public get frame(): number {
        return this._frame;
    }

    /** Returns the current frames per second rendered */
    public get fps(): number {
        return this._fps;
    }

    /** Returns the time the last update was called */
    public get lastUpdate(): number {
        return this._lastUpdate;
    }

    /** Returns the current delta time */
    public get delta(): number {
        return this._delta;
    }

    constructor(options: EngineOptions = { scene: undefined }) {
        this._running = false;
        this._frame = 0;
        this._delta = 0;
        this._fps = 0;
        this._lastUpdate = 0;
        this.rafId = 0;

        this.scene = options.scene;
    }

    /** Starts the engine, starting the scene and starts the core loop */
    public start(): void {
        if (!this.running && this.scene) {
            this._running = true;

            this.scene.start();

            this.rafId = requestAnimationFrame(this.update.bind(this));
        }
    }

    /** Stops the core loop */
    public stop(): void {
        if (this.running) {
            this._running = false;
            cancelAnimationFrame(this.rafId);
        }
    }

    private update(timestamp: number): void {
        if (this.scene === undefined) {
            this.stop();
            return;
        }

        this._frame++;
        this._delta = (timestamp - this.lastUpdate) / 1000;
        this._fps = 1 / this.delta;
        this._lastUpdate = timestamp;

        this.scene.update(this.delta);

        this.rafId = requestAnimationFrame(this.update.bind(this));
    }
}
