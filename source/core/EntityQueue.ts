import { Entity } from "./Entity";

export type EntityQueue = { [query: string]: Entity[] };
