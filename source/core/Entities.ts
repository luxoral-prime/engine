import { Entity } from "./Entity";
import { Component, ComponentConstructor } from "./Component";
import { Query, QueryOperator } from "./Query";
import { Scene } from "./Scene";

/** A container for entities */
export class Entities {
    private _scene: Scene;
    private _entities: Map<string, Entity>;

    /** Returns the number of entities contained in this class */
    public get length(): number {
        return this._entities.size;
    }

    constructor(scene: Scene, entities: Entity[] = []) {
        this._scene = scene;
        this._entities = new Map();

        for (const entity of entities) {
            this.add(entity);
        }
    }

    /** Checks if this contains an Entity with the specified name */
    public has(name: string | Entity): boolean {
        return this._entities.has(name instanceof Entity ? name.name : name);
    }

    /** Returns the specified Entity from this container */
    public get(name: string | Entity): Entity | undefined {
        return this._entities.get(name instanceof Entity ? name.name : name);
    }

    /** Checks if given Entity matches the specified Queries */
    public entityMatchesQueries<T = never>(
        entity: Entity,
        queries: Query<T>[]
    ): boolean {
        let matches = true;

        for (const query of queries) {
            switch (query.operator) {
                case QueryOperator.Has:
                    if (
                        !entity.components.has(
                            query.operand as ComponentConstructor<T>
                        )
                    ) {
                        matches = false;
                    }
                    break;
                case QueryOperator.Lacks:
                    if (
                        entity.components.has(
                            query.operand as ComponentConstructor<T>
                        )
                    ) {
                        matches = false;
                    }
                    break;
                case QueryOperator.Named:
                    if (entity.name !== (query.operand as string)) {
                        matches = false;
                    }
                    break;
            }
        }

        return matches;
    }

    /** Returns all Entities that matches the specified Queries */
    public matchesQueries<T = never>(queries: Query<T>[]): Entity[] {
        const entities = [];

        for (const [, entity] of this._entities) {
            if (this.entityMatchesQueries(entity, queries)) {
                entities.push(entity);
            }
        }

        return entities;
    }

    /** Adds an entity to this container */
    public add(entity: Entity): void {
        if (!this.has(entity)) {
            this._entities.set(entity.name, entity);

            this._scene.systems.componentEnter(entity);
        }
    }

    /** Removes an entity from this container */
    public remove(name: string | Entity): boolean {
        const entity = this.get(name);
        if (entity) {
            this._scene.systems.componentExit(entity);
        }

        return this._entities.delete(name instanceof Entity ? name.name : name);
    }
}
