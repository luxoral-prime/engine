import { System } from "./System";
import { Query, QueryOperator } from "./Query";
import { EntityQueue } from "./EntityQueue";
import { Scene } from "./Scene";
import { Entity } from "./Entity";

/** A container for systems */
export class Systems {
    private _scene: Scene;
    private _systems: Map<string, System>;

    /** Returns the number of systems contained in this class */
    public get length(): number {
        return this._systems.size;
    }

    constructor(scene: Scene, systems: System[] = []) {
        this._scene = scene;
        this._systems = new Map();

        for (const system of systems) {
            this.add(system);
        }
    }

    private getEntityQueue(system: System): EntityQueue {
        const entityQueue: EntityQueue = {};

        for (const identifier in system.queries) {
            const queries: Query<any>[] = system.queries[identifier].map((q) =>
                typeof q === "function"
                    ? { operator: QueryOperator.Has, operand: q }
                    : q
            );

            const matchedEntities = this._scene.entities.matchesQueries(
                queries
            );
            entityQueue[identifier] = matchedEntities;
        }

        return entityQueue;
    }

    /** Checks if this contains an System with the specified name */
    public has(name: string | System): boolean {
        return this._systems.has(
            name instanceof System ? name.constructor.name : name
        );
    }

    /** Returns the specified System from this container */
    public get(name: string | System): System | undefined {
        return this._systems.get(
            name instanceof System ? name.constructor.name : name
        );
    }

    /** Adds an system to this container */
    public add(system: System): void {
        if (!this.has(system)) {
            this._systems.set(system.constructor.name, system);
            system.added?.(this.getEntityQueue(system));
        }
    }

    /** Removes an system from this container */
    public remove(name: string | System): boolean {
        const system = this.get(name);
        system?.removed?.(this.getEntityQueue(system));

        return this._systems.delete(
            name instanceof System ? name.constructor.name : name
        );
    }

    /** Start a system in this container */
    public start(name: string | System): void {
        const system = this.get(name);

        system?.start?.(this.getEntityQueue(system));
    }

    /** Start all systems in this container */
    public startAll(): void {
        for (const [, system] of this._systems) {
            this.start(system);
        }
    }

    /** Update a system in this container */
    public update(name: string | System, delta: number): void {
        const system = this.get(name);

        system?.update?.(this.getEntityQueue(system), delta);
    }

    /** Update all systems in this container */
    public updateAll(delta: number): void {
        for (const [, system] of this._systems) {
            this.update(system, delta);
        }
    }

    public componentEnter(entity: Entity): void {
        for (const [, system] of this._systems) {
            for (const identifier in system.queries) {
                const queries: Query<any>[] = system.queries[
                    identifier
                ].map((q) =>
                    typeof q === "function"
                        ? { operator: QueryOperator.Has, operand: q }
                        : q
                );

                if (
                    this._scene.entities.entityMatchesQueries(entity, queries)
                ) {
                    system.enter?.(entity);
                }
            }
        }
    }

    public componentExit(entity: Entity): void {
        for (const [, system] of this._systems) {
            for (const identifier in system.queries) {
                const queries: Query<any>[] = system.queries[
                    identifier
                ].map((q) =>
                    typeof q === "function"
                        ? { operator: QueryOperator.Has, operand: q }
                        : q
                );

                if (
                    this._scene.entities.entityMatchesQueries(entity, queries)
                ) {
                    system.exit?.(entity);
                }
            }
        }
    }
}
