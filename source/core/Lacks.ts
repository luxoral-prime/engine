import { ComponentConstructor } from "./Component";
import { Query, QueryOperator } from "./Query";

export function lacks<T>(component: ComponentConstructor<T>): Query<T> {
    return {
        operator: QueryOperator.Lacks,
        operand: component
    } as Query<T>;
}
