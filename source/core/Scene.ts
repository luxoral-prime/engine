import { Entity } from "./Entity";
import { System } from "./System";
import { Entities } from "./Entities";
import { Systems } from "./Systems";

/** The Scene class containing and handling Entities and Systems */
export class Scene {
    public readonly name: string;

    /** All of this Scenes entities */
    public entities: Entities;
    /** All of this Scenes systems */
    public systems: Systems;

    constructor(name: string, entities: Entity[] = [], systems: System[] = []) {
        this.name = name;
        this.entities = new Entities(this, entities);
        this.systems = new Systems(this, systems);
    }

    /** A method for starting all of this Scenes systems on all of this Scenes Entities */
    public start(): void {
        this.systems.startAll();
    }

    /** A method for updating all of this Scenes systems on all of this Scenes Entities */
    public update(delta: number): void {
        this.systems.updateAll(delta);
    }
}
