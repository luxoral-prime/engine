import { Query, QueryOperator } from "./Query";

export function named(name: string): Query {
    return {
        operator: QueryOperator.Named,
        operand: name
    } as Query;
}
