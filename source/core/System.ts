import { ComponentConstructor } from "./Component";
import { Query } from "./Query";
import { EntityQueue } from "./EntityQueue";
import { Entity } from "./Entity";

/** The class all Systems extend  */
export abstract class System {
    /** All of the Entities this system will be querying */
    public abstract queries: {
        [query: string]: (Query<any> | ComponentConstructor<any>)[];
    };

    /** A method that is called once the Scene is started */
    public start?(entities: EntityQueue): void;

    /** A method that is called once every time the Scene updates */
    public update?(entities: EntityQueue, delta: number): void;

    /** A method that is called once the system is added */
    public added?(entities: EntityQueue): void;
    /** A method that is called once the system is removed */
    public removed?(entities: EntityQueue): void;

    /** A method that is called once a new entity that matches a query is added */
    public enter?(entity: Entity): void;
    /** A method that is called once a new entity that matches a query is removed */
    public exit?(entitiy: Entity): void;
}
