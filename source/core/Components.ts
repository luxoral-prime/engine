import { Component, ComponentConstructor } from "./Component";

type ComponentMap<K, V extends Component> = Map<K, V>;

/** A container for Components */
export class Components {
    private _components: ComponentMap<string, any>;

    /** Returns the number of components contained in this class */
    public get length(): number {
        return this._components.size;
    }

    constructor(components: ComponentConstructor<any>[] = []) {
        this._components = new Map();

        this.addMultiple(components);
    }

    /** Checks if this contains the specified ComponentConstructor */
    public has<T extends Component>(
        component: ComponentConstructor<T>
    ): boolean {
        return this._components.has(component.name);
    }

    /** Returns a Component of the same type as the specified ComponentConstructor */
    public get<T extends Component>(component: ComponentConstructor<T>): T {
        return this._components.get(component.name);
    }

    /** Adds a new Component from the specified ComponentConstructor with the rest of the parameters being passed to the constructor */
    public add<T extends Component>(
        component: ComponentConstructor<T>,
        ...args: any[]
    ): void {
        if (!this.has(component)) {
            this._components.set(component.name, new component(...args));
        }
    }

    /** Add multiple Components from an array of ComponentConstructors */
    public addMultiple<T extends Component>(
        components: ComponentConstructor<T>[]
    ): void {
        for (const component of components) {
            this.add(component);
        }
    }

    /** Removes a Component of the same type as the specified ComponentConstructor */
    public remove<T extends Component>(
        component: ComponentConstructor<T>
    ): boolean {
        return this._components.delete(component.name);
    }
}
