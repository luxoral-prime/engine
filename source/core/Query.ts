import { ComponentConstructor } from "./Component";

export enum QueryOperator {
    Has,
    Lacks,
    Named
}

export type Query<T = never> = {
    operator: QueryOperator;
    operand: ComponentConstructor<T> | string;
};
