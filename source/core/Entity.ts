import { Components } from "./Components";
import { ComponentConstructor } from "./Component";

/** The Entity class containing Components and a name */
export class Entity {
    /** This Entities name (basically an identifier) */
    public name: string;
    /** The Components this Entity contains */
    public components: Components;

    constructor(name: string, components: ComponentConstructor<any>[] = []) {
        this.name = name;
        this.components = new Components(components);
    }
}
