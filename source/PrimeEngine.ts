/** The current version of the Engine */
export const version = "0.1.14";

((): void =>
    console.log(
        `%c
  _____       _              _____            _
 |  _  | ___ |_| _____  ___ |   __| ___  ___ |_| ___  ___
 |   __||  _|| ||     || -_||   __||   || . || ||   || -_|
 |__|   |_|  |_||_|_|_||___||_____||_|_||_  ||_||_|_||___|
                                        |___|
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Made for Luxoral Prime
Made by @Snabel#5886
Made with <3 (and a bit of frustration)

Version ${version}

Open source btw -> https://gitlab.com/luxoral-prime/engine
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
`
    ))();

export * from "./core/Component";
export * from "./core/Components";
export * from "./core/Engine";
export * from "./core/Entities";
export * from "./core/Entity";
export * from "./core/EntityQueue";
export * from "./core/Lacks";
export * from "./core/Named";
export * from "./core/Query";
export * from "./core/Scene";
export * from "./core/System";
export * from "./core/Systems";
